/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/Apps/SparkFSApp/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->c.fs  */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <ctype.h>
#include <time.h>

#include "os.h"
#include "wimp.h"
#include "sprite.h"
#include "wimpt.h"
#include "bbc.h"
#include "kernel.h"

#include "Interface/SparkFS.h"

#include "wos.h"
#include "sparkfs.h"
#include "ram.h"
#include "fs.h"

/*****************************************************************************/




os_error * movefile(char *  from,char * to)
{
 char string[512];
 os_error * errpoi=NULL;

 if(rename(from,to))
 {
  sprintf(string,"copy %s %s a~cdf~l~n~q~r~s~T~v",from,to); /* using d opt */
  errpoi=os_cli(string);
 }

 return(errpoi);
}



static char * scrappath="cdir <Spark$Scrap>.SparkFSTmp";

static int validscrap(void)
{
 os_error * ep;

 ep=NULL;

 if(!fexists(scrappath+5)) ep=os_cli(scrappath);

 return(ep==NULL);
}



/* input old name in arcname, output new name in newname */
/* name is in same directory unless scrap is true        */

void makenames(char * arcname,char * newname,int scrap)
{
 char * p;
 char   temp[32];
 int    lentemp;
 int    lenfile;
 int    i;

 if(getenv("Spark$Scrap") && scrap && validscrap())
 {
  strcpy(newname,"<Spark$Scrap>.SparkFSTmp.");
  p=leaf(arcname);
  strcat(newname,p);
 }
 else
 {
  strcpy(newname,arcname);
 }

 p=leaf(newname);
 lenfile=strlen(p);

 for(i=0;i<1000;i++)
 {
  if((!fexists(newname))) break;
  sprintf(temp,"%02d",i);
  lentemp=strlen(temp);
  if((lentemp+lenfile)>10) lenfile=10-lentemp;
  strcpy(p+lenfile,temp);
 }
}



void makename(char * arcname,char * newname)
{
 makenames(arcname,newname,1);
}



/* convert a filename into an acceptable RISCOS one */

char * convert(const char * filename)
{
 static char newfile[64];
 int         i, j, len;

 strncpy(newfile,filename,sizeof(newfile)-1);
 newfile[sizeof(newfile)-1]=0;

 j=0;
 len=strlen(newfile);
 for(i=0;i<len;i++)
 {
  switch(newfile[i])
  {
            case  32:
                     /* Compact any spaces */
                     continue;

            case '.':
                     newfile[j]='/';
                     break;

            case '"':
            case '#':
            case '$':
            case '%':
            case '&':
            case '*':
            case ':':
            case '@':
            case'\\':
            case '^':
            case '|':
            case 127:
                     newfile[j]='X';
                     break;

            default :
                     if(newfile[i]<32) newfile[j]='X';
                     else              newfile[j]=newfile[i];
                     break;
  }
  j++;
 }

 newfile[j]=0; /* Trim end where spaces were compacted */
 if(strlen(newfile)==0) strcpy(newfile,"DEFAULT");

 return(newfile);
}
